package fr.sfeir.aoc;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeDay1IT extends Day1Test {

    // Execute the same tests but in native mode.
}
